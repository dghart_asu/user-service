#!/bin/sh 

./mvnw clean package -DskipTests
mkdir build_tmp
cp target/user-*.jar build_tmp/
cp -r .ebextensions build_tmp/
cd build_tmp
zip -r ../deployment.zip .
