package com.dustinhart.asu.users.resources.v1;

import com.dustinhart.asu.users.exceptions.ServerException;
import com.dustinhart.asu.users.models.User;
import com.dustinhart.asu.users.resources.v1.dtos.*;
import com.dustinhart.asu.users.security.JwtTokenProvider;
import com.dustinhart.asu.users.security.UserPrincipal;
import com.dustinhart.asu.users.services.UserService;
import io.jsonwebtoken.JwtException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/ws/v1/users")
@CrossOrigin(origins = "*")
@RequiredArgsConstructor
@Slf4j
public class UserResource {

    private final AuthenticationManager authenticationManager;
    private final UserService userService;
    private final PasswordEncoder passwordEncoder;

    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<?> listUsers() {
        List<UserResponse> users = userService.getUsers().stream()
                .map(u -> new UserResponse(u.getEmail(), u.getFirstName(), u.getLastName()))
                .collect(Collectors.toList());
        return ResponseEntity.ok(users);
    }

    private final JwtTokenProvider tokenProvider;

    @RequestMapping(value = "/register", method = RequestMethod.POST)
    public ResponseEntity<?> createUser(@RequestBody CreateUserRequest request) {
        log.info("Creating user with email {}", request.getEmail());

        // Create user with random password
        User user = new User();
        user.setEmail(request.getEmail());
        user.setFirstName(request.getFirstName());
        user.setLastName(request.getLastName());
        user.setEncryptedPassword(passwordEncoder.encode(UUID.randomUUID().toString()));
        userService.createUser(user);

        UserResponse response = new UserResponse(
                user.getEmail(),
                user.getFirstName(),
                user.getLastName()
        );

        return ResponseEntity.ok(response);
    }

    @RequestMapping(value = "/setPassword", method = RequestMethod.POST)
    public ResponseEntity<?> setPassword(@RequestBody SetPasswordRequest request) {
        try {
            String encryptedPassword = passwordEncoder.encode(request.getPassword());
            userService.setUserPassword(request.getToken(), encryptedPassword);
        } catch (Exception e) {
            log.warn("Failed to set the password for a user");
            return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
        }

        return ResponseEntity.ok().build();
    }

    @RequestMapping(value = "/changePassword", method = RequestMethod.POST)
    public ResponseEntity<?> changePassword(@RequestBody ChangePasswordRequest request) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        UserPrincipal principal = (UserPrincipal) authentication.getPrincipal();

        String email = principal.getEmail();

        // throws exception on failure
        authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(email, request.getOldPassword()));

        try {
            String encryptedPassword = passwordEncoder.encode(request.getNewPassword());
            userService.changeUserPassword(email, encryptedPassword);
        } catch (Exception e) {
            log.warn("Failed to change the password for a user");
            return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
        }

        return ResponseEntity.ok().build();
    }

    @RequestMapping(value = "/resetPassword", method = RequestMethod.POST)
    public ResponseEntity<?> resetPassword(@RequestBody ForgotPasswordRequest request) {
        userService.sendPasswordReset(request.getEmail());

        // Don't indicate if the user exists
        return ResponseEntity.ok().build();
    }

    @RequestMapping(value = "/authenticate", method = RequestMethod.POST)
    public ResponseEntity<?> authenticate(@RequestBody LoginRequest request) {
        log.debug("Received authenticate request for user {} with password {}", request.getEmail(), request.getPassword());
        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        request.getEmail(),
                        request.getPassword()
                )
        );

        SecurityContextHolder.getContext().setAuthentication(authentication);

        String jwt = tokenProvider.generateToken(authentication);
        return ResponseEntity.ok(new JwtResponse(jwt));
    }

    @RequestMapping(value = "/validate", method = RequestMethod.POST)
    public ResponseEntity<?> validate(@RequestBody JwtRequest request) {
        String username = tokenProvider.getJwtClaims(request.getJwt()).get("email", String.class);
        log.debug("Validate jwt request for user {}", username);

        User u = userService.findUserById(username);
        if (u != null && tokenProvider.validateToken(request.getJwt())) {
            return ResponseEntity.ok().build();
        } else {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        }
    }

    @ExceptionHandler(JwtException.class)
    public ResponseEntity<?> handleException(JwtException e) {
        log.info("Unable to handle jwt: {}", e.getMessage());
        return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
    }

    @ExceptionHandler(ServerException.class)
    public ResponseEntity<?> handleException(ServerException e) {
        log.info("Server Error: {}", e.getMessage());
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
    }
}
