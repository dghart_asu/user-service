package com.dustinhart.asu.users.resources.v1.dtos;

import lombok.Data;

@Data
public class LoginRequest {
    private String email;
    private String password;
}
