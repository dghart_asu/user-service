package com.dustinhart.asu.users.resources.v1;

import com.dustinhart.asu.users.models.User;
import com.dustinhart.asu.users.resources.v1.dtos.CreateUserRequest;
import com.dustinhart.asu.users.resources.v1.dtos.SendEmailRequest;
import com.dustinhart.asu.users.resources.v1.dtos.UserResponse;
import com.dustinhart.asu.users.services.EmailService;
import com.dustinhart.asu.users.services.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@RequestMapping("/ws/v1/emails")
@CrossOrigin(origins = "*")
@RequiredArgsConstructor
@Slf4j
public class EmailResource {

    @Value("${app.urls.baseUrl}")
    private String baseUrl;

    private final EmailService emailService;
    private final UserService userService;

    @RequestMapping(value = "/summaryReport", method = RequestMethod.POST)
    public ResponseEntity<?> sendSummaryReportPosted(@RequestBody SendEmailRequest request) {
        log.info("Sending summary report notice to user with email {}", request.getTargetEmail());

        User user = userService.findUserById(request.getTargetEmail());
        if(user == null) {
            String message = "User with email address " + request.getTargetEmail() + " not found";
            log.warn(message + " when sending summary report notice");
            return ResponseEntity.status(404).body(message);
        }

        Map<String, Object> attributes = request.getAttributes();
        attributes.put("baseUrl", baseUrl);

        emailService.sendSummaryReportEmail(request.getTargetEmail(), request.getAttributes());

        return ResponseEntity.ok().build();
    }

    @RequestMapping(value = "/activityReminder", method = RequestMethod.POST)
    public ResponseEntity<?> sendActivityReminder(@RequestBody SendEmailRequest request) {
        log.info("Sending activity reminder to user with email {}", request.getTargetEmail());

        User user = userService.findUserById(request.getTargetEmail());
        if(user == null) {
            String message = "User with email address " + request.getTargetEmail() + " not found";
            log.warn(message + " when sending summary report notice");
            return ResponseEntity.status(404).body(message);
        }

        Map<String, Object> attributes = request.getAttributes();
        attributes.put("baseUrl", baseUrl);

        emailService.sendActivityReminderEmail(request.getTargetEmail(), request.getAttributes());

        return ResponseEntity.ok().build();
    }

}