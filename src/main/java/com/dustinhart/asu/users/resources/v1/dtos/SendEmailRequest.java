package com.dustinhart.asu.users.resources.v1.dtos;

import lombok.Data;

import java.util.Map;

@Data
public class SendEmailRequest {
    private String targetEmail;
    private Map<String, Object> attributes;
}
