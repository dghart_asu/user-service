package com.dustinhart.asu.users.resources.v1.dtos;

import lombok.Data;

@Data
public class CreateUserRequest {
    private String email;
    private String firstName;
    private String lastName;
}
