package com.dustinhart.asu.users.resources.v1.dtos;

import lombok.Data;

@Data
public class ForgotPasswordRequest {
    private String email;
}
