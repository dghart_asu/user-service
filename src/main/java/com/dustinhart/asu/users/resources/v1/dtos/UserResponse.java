package com.dustinhart.asu.users.resources.v1.dtos;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public class UserResponse {
    private final String email;
    private final String firstName;
    private final String lastName;
}
