package com.dustinhart.asu.users.security;

import com.dustinhart.asu.users.models.User;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

@Data
public class UserPrincipal implements UserDetails {
    private String id;

    private String name;

    private String username;

    private String email;

    @JsonIgnore
    private String password;

    private Collection<? extends GrantedAuthority> authorities;

    public static UserPrincipal create(User entity) {
        List<GrantedAuthority> authorities = Collections.singletonList(new SimpleGrantedAuthority("User"));

        UserPrincipal p = new UserPrincipal();
        p.setId(entity.getEmail());
        p.setEmail(entity.getEmail());
        p.setUsername(entity.getEmail());
        p.setName(String.format("%s %s", entity.getFirstName(), entity.getLastName()));
        p.setPassword(entity.getEncryptedPassword());
        p.setAuthorities(authorities);

        return p;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }
}
