package com.dustinhart.asu.users.exceptions;

public class UserNotFoundException extends RuntimeException {
    public UserNotFoundException() {
        super("A user with count not be found");
    }
}
