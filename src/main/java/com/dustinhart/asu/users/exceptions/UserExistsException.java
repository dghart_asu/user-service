package com.dustinhart.asu.users.exceptions;

public class UserExistsException extends RuntimeException {

    public UserExistsException(String email) {
        super("A user with the email " + email + " already exists.");
    }
}
