package com.dustinhart.asu.users.models;

import lombok.Data;

@Data
public class EmailTemplate {
    String subject;
    String content;
}
