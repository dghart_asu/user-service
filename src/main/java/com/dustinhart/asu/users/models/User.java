package com.dustinhart.asu.users.models;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.Date;

@Data
@Entity(name = "users")
public class User {
    @Id
    private String email;
    private String firstName;
    private String lastName;
    private String encryptedPassword;
    private String passwordResetToken;
    private Date passwordResetDate;
}
