package com.dustinhart.asu.users.services.impl;

import com.amazonaws.services.simpleemail.AmazonSimpleEmailService;
import com.amazonaws.services.simpleemail.model.*;
import com.dustinhart.asu.users.exceptions.ServerException;
import com.dustinhart.asu.users.services.EmailClient;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

// https://docs.aws.amazon.com/ses/latest/DeveloperGuide/send-using-sdk-java.html
@Component
@Profile("prod")
@Slf4j
@RequiredArgsConstructor
public class AmazonEmailClient implements EmailClient {

    @Value("${app.emails.fromAddress}")
    private String fromAddress;

    private final AmazonSimpleEmailService sesClient;

    @Override
    public void sendEmail(String target, String subject, String content) {
        final String TO = target;

        try {
            log.info("Sending email through Amazon SES to " + TO);

            SendEmailRequest request = new SendEmailRequest()
                    .withSource(fromAddress)
                    .withDestination(new Destination().withToAddresses(TO))
                    .withMessage(createMessage(content, content)
                    .withSubject(new Content().withCharset("UTF-8").withData(subject)));

            sesClient.sendEmail(request);

            log.info("Email sent successfully");
        } catch (Exception ex) {
            log.error("The email was not sent. Error message: " + ex.getMessage());
            throw new ServerException("Unable to send email. Please try again later.");
        }
    }

    private Message createMessage(String html, String text) {
        return new Message().withBody(new Body()
                .withHtml(new Content().withCharset("UTF-8").withData(html))
                .withText(new Content().withCharset("UTF-8").withData(text)));
    }
}
