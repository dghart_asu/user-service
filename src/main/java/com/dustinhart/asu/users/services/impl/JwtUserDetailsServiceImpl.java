package com.dustinhart.asu.users.services.impl;

import com.dustinhart.asu.users.models.User;
import com.dustinhart.asu.users.security.UserPrincipal;
import com.dustinhart.asu.users.services.JwtUserDetailsService;
import com.dustinhart.asu.users.services.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class JwtUserDetailsServiceImpl implements JwtUserDetailsService {

    private final UserService userService;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        return loadUserById(username);
    }

    public UserDetails loadUserById(String id) throws UsernameNotFoundException {
        User user = userService.findUserById(id);
        if (user != null) {
            return UserPrincipal.create(user);
        }

        throw new UsernameNotFoundException("Unable to find user with email: " + id);
    }
}
