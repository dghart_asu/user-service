package com.dustinhart.asu.users.services;

public interface EmailClient {
    void sendEmail(String target, String subject, String content);
}
