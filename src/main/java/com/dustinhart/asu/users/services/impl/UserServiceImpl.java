package com.dustinhart.asu.users.services.impl;

import com.dustinhart.asu.users.exceptions.UserExistsException;
import com.dustinhart.asu.users.exceptions.UserNotFoundException;
import com.dustinhart.asu.users.models.User;
import com.dustinhart.asu.users.repos.UserRepository;
import com.dustinhart.asu.users.services.EmailService;
import com.dustinhart.asu.users.services.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
@RequiredArgsConstructor
@Slf4j
public class UserServiceImpl implements UserService {

    private final UserRepository repo;
    private final EmailService emailService;

    @Value("${app.urls.baseUrl}")
    private String baseUrl;

    @Override
    public List<User> getUsers() {
        return repo.findAll();
    }

    @Override
    public User createUser(User user) {
        Optional<User> existingUser = repo.findById(user.getEmail());
        if (existingUser.isPresent()) {
            throw new UserExistsException(user.getEmail());
        }

        user.setPasswordResetToken(UUID.randomUUID().toString());
        user.setPasswordResetDate(new Date());

        user = repo.save(user);

        Map<String, Object> emailAttributes = new HashMap<>();
        emailAttributes.put("token", user.getPasswordResetToken());
        emailAttributes.put("expirationDate", user.getPasswordResetDate());
        emailAttributes.put("baseUrl", baseUrl);

        emailService.sendWelcomeEmail(user.getEmail(), emailAttributes);

        return user;
    }

    public void changeUserPassword(String email, String encryptedPassword) {
        Optional<User> existingUser = repo.findById(email);
        if (existingUser.isEmpty()) {
            throw new UserNotFoundException();
        }

        User user = existingUser.get();
        user.setEncryptedPassword(encryptedPassword);

        repo.save(user);

        log.info("Password was changed for user {}", user.getEmail());

        Map<String, Object> emailAttributes = new HashMap<>();
        emailAttributes.put("baseUrl", baseUrl);

        emailService.sendPasswordWasChangedEmail(user.getEmail(), emailAttributes);
    }

    public void setUserPassword(String token, String encryptedPassword) {
        Optional<User> existingUser = repo.findByPasswordResetToken(token);
        if (existingUser.isEmpty()) {
            throw new UserNotFoundException();
        }

        User user = existingUser.get();
        user.setPasswordResetToken(null);
        user.setPasswordResetDate(null);
        user.setEncryptedPassword(encryptedPassword);

        repo.save(user);

        log.info("New password was set for user {}", user.getEmail());

        Map<String, Object> emailAttributes = new HashMap<>();
        emailAttributes.put("baseUrl", baseUrl);

        emailService.sendPasswordWasResetEmail(user.getEmail(), emailAttributes);
    }

    @Override
    public void sendPasswordReset(String email) {
        Optional<User> existingUser = repo.findById(email);
        if (existingUser.isEmpty()) {
            log.info("Received password reset request for unknown email {}", email);
            return;
        }

        User user = existingUser.get();
        user.setPasswordResetToken(UUID.randomUUID().toString());
        user.setPasswordResetDate(new Date());

        repo.save(user);

        log.info("Password reset for user {}", user.getEmail());

        Map<String, Object> emailAttributes = new HashMap<>();
        emailAttributes.put("token", user.getPasswordResetToken());
        emailAttributes.put("expirationDate", user.getPasswordResetDate());
        emailAttributes.put("baseUrl", baseUrl);

        emailService.sendPasswordResetEmail(user.getEmail(), emailAttributes);
    }

    @Override
    public User findUserById(String id) {
        return repo.findById(id).orElse(null);
    }
}
