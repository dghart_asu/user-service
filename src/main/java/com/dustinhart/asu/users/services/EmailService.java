package com.dustinhart.asu.users.services;

import java.util.Map;

public interface EmailService {
    void sendWelcomeEmail(String target, Map<String, Object> attributes);
    void sendPasswordWasResetEmail(String target, Map<String, Object> attributes);
    void sendPasswordWasChangedEmail(String target, Map<String, Object> attributes);
    void sendPasswordResetEmail(String target, Map<String, Object> attributes);
    void sendSummaryReportEmail(String target, Map<String, Object> attributes);
    void sendActivityReminderEmail(String target, Map<String, Object> attributes);
}
