package com.dustinhart.asu.users.services.impl;

import com.dustinhart.asu.users.models.EmailTemplate;
import com.dustinhart.asu.users.services.EmailTemplateService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Service;

import java.io.*;
import java.nio.file.Files;
import java.util.Map;
import java.util.regex.Pattern;

@Service
@Slf4j
@RequiredArgsConstructor
public class EmailTemplateServiceImpl implements EmailTemplateService {

    @Value("${app.emails.templateBase}")
    private String templateBase;

    public EmailTemplate loadTemplate(String templateName, String subjectTemplate, Map<String, Object> attributes) {
        EmailTemplate template = new EmailTemplate();

        template.setSubject(replaceVariables(subjectTemplate, attributes));
        template.setContent(replaceVariables(loadFile(templateName), attributes));

        return template;
    }

    private String loadFile(String templateName) {
        String contents = null;
        try (
                InputStream in = getClass().getResourceAsStream(templateBase + templateName + ".html");
                BufferedReader reader = new BufferedReader(new InputStreamReader(in))
        ){
            StringBuilder sb = new StringBuilder();
            String line;
            while((line = reader.readLine()) != null) {
                sb.append(line);
                sb.append("\n");
            }
            contents = sb.toString();
        } catch (IOException e) {
            log.error("Error while attempting to read email template", e);
        }

        return contents;
    }

    /**
     * replace all instances of {{keyName}} with the value from the attribute map
     * @param content the string with variables to replace
     * @param attributes the key/values to use
     * @return the content with the variables replaced
     */
    private String replaceVariables(String content, Map<String, Object> attributes) {
        for(String key : attributes.keySet()) {
            content = content.replaceAll(
                    Pattern.quote("{{" + key + "}}"),
                    attributes.get(key).toString()
            );
        }
        return content;
    }
}
