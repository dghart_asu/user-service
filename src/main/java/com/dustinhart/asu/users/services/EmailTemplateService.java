package com.dustinhart.asu.users.services;

import com.dustinhart.asu.users.models.EmailTemplate;

import java.util.Map;

public interface EmailTemplateService {
    EmailTemplate loadTemplate(String templateName, String subjectTemplate, Map<String, Object> attributes);
}
