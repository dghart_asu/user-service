package com.dustinhart.asu.users.services.impl;

import com.dustinhart.asu.users.services.EmailClient;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;


@Component
@Profile("!prod")
@Slf4j
public class NoSendEmailClient implements EmailClient {
    @Override
    public void sendEmail(String target, String subject, String content) {
        log.info("The following email is not being sent because profile is not set to 'prod'");

        log.info("Target: " + target);
        log.info("Subject: " + subject);
        log.info("Content: " + content);
    }
}
