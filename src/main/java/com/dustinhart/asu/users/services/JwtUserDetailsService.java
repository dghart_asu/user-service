package com.dustinhart.asu.users.services;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

/**
 * This custom implementation is used by Spring Security to load users before their
 * passwords are checked. The concrete class could implement UserDetailsService
 * directly, but creating an intermediate interface allows for better testing.
 */
public interface JwtUserDetailsService extends UserDetailsService {
    UserDetails loadUserById(String id) throws UsernameNotFoundException;
    UserDetails loadUserByUsername(String username) throws UsernameNotFoundException;
}
