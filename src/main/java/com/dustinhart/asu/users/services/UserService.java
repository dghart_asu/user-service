package com.dustinhart.asu.users.services;

import com.dustinhart.asu.users.models.User;

import java.util.List;

/*
  Domain level service for performing basic operations with users.
 */
public interface UserService {
    /**
     * Returns a list of all users.
     * @return all users
     */
    List<User> getUsers();

    /**
     * Creates a new user if one doesn't exist with a matching email address.
     * All fields are required and this method will through an exception if
     * unable to create the user.  The user's password should be encoded.
     * @param user the user's details
     * @return the same object used to create the user
     */
    User createUser(User user);

    /**
     * Used to set a new password for a user. The user will be sent an email
     * notifying them of the change.
     * @param email
     * @param encryptedPassword
     */
    void changeUserPassword(String email, String encryptedPassword);

    /**
     *
     * @param token the single user password reset token
     * @param encryptedPassword an encrypted version of the new password
     */
    void setUserPassword(String token, String encryptedPassword);

    /**
     * Send a Password Reset email if the user exists, otherwise do nothing.
     * @param email
     */
    void sendPasswordReset(String email);

    /**
     * Looks up a user by their email address.
     * @param id the id of the user to search for
     * @return an existing User with the matching email, otherwise null
     */
    User findUserById(String id);
}
