package com.dustinhart.asu.users.services.impl;

import com.dustinhart.asu.users.models.EmailTemplate;
import com.dustinhart.asu.users.services.EmailClient;
import com.dustinhart.asu.users.services.EmailService;
import com.dustinhart.asu.users.services.EmailTemplateService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
@RequiredArgsConstructor
@Slf4j
public class EmailServiceImpl implements EmailService {

    private final EmailTemplateService emailTemplateService;
    private final EmailClient emailClient;

    @Override
    public void sendWelcomeEmail(String target, Map<String, Object> attributes) {
        EmailTemplate template = emailTemplateService.loadTemplate(
                "welcome",
                "Finish signing up for Productivity Tracker",
                attributes);

        emailClient.sendEmail(target, template.getSubject(), template.getContent());
    }

    @Override
    public void sendPasswordWasChangedEmail(String target, Map<String, Object> attributes) {
        EmailTemplate template = emailTemplateService.loadTemplate(
                "password_was_changed",
                "Your Password was Changed",
                attributes);

        emailClient.sendEmail(target, template.getSubject(), template.getContent());
    }

    @Override
    public void sendPasswordWasResetEmail(String target, Map<String, Object> attributes) {
        EmailTemplate template = emailTemplateService.loadTemplate(
                "password_was_reset",
                "Your Password was Reset",
                attributes);

        emailClient.sendEmail(target, template.getSubject(), template.getContent());
    }

    @Override
    public void sendPasswordResetEmail(String target, Map<String, Object> attributes) {
        EmailTemplate template = emailTemplateService.loadTemplate(
                "password_reset",
                "Reset your password",
                attributes);

        emailClient.sendEmail(target, template.getSubject(), template.getContent());
    }

    @Override
    public void sendSummaryReportEmail(String target, Map<String, Object> attributes) {
        EmailTemplate template = emailTemplateService.loadTemplate(
                "summary_report_posted",
                "New Summary Report",
                attributes);

        emailClient.sendEmail(target, template.getSubject(), template.getContent());
    }

    @Override
    public void sendActivityReminderEmail(String target, Map<String, Object> attributes) {
        EmailTemplate template = emailTemplateService.loadTemplate(
                "activity_reminder",
                "Updated Report Needed",
                attributes);

        emailClient.sendEmail(target, template.getSubject(), template.getContent());
    }
}
