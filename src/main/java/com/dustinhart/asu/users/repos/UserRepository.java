package com.dustinhart.asu.users.repos;

import com.dustinhart.asu.users.models.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User, String> {
    Optional<User> findByPasswordResetToken(String token);
}
