package com.dustinhart.asu.users.config;

import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.simpleemail.AmazonSimpleEmailService;
import com.amazonaws.services.simpleemail.AmazonSimpleEmailServiceClientBuilder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AwsConfig {
    @Value("${app.aws.iam.accessKey}")
    private String accessKey;

    @Value("${app.aws.iam.secretKey}")
    private String secretKey;

    @Bean
    public AWSCredentialsProvider awsCredentialsProvider() {
        // https://docs.aws.amazon.com/sdk-for-java/v1/developer-guide/credentials.html
        return new AWSStaticCredentialsProvider(new BasicAWSCredentials(accessKey, secretKey));
    }

    @Bean
    public AmazonSimpleEmailService amazonSimpleEmailServiceClient() {
        return AmazonSimpleEmailServiceClientBuilder
                .standard()
                .withCredentials(awsCredentialsProvider())
                .withRegion(Regions.US_EAST_1)
                .build();
    }
}
