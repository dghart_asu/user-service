FROM ubuntu:18.04
RUN apt-get update -y && apt-get install openjdk-11-jdk maven -y
COPY . /app
WORKDIR /app
RUN mvn clean package -DskipTests
EXPOSE 8080
CMD java -jar target/user-service-*.jar --spring.profiles.active=prod